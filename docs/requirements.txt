# Copied from pyproject.toml

Sphinx ~= 3.5.4
sphinx-rtd-theme ~= 0.5.2
sphinx-copybutton ~= 0.3.1
myst-parser ~= 0.14.0
